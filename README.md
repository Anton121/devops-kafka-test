# Пооднятие kafka cluster из 3х брокеров и одного zookeeper
------------------------------------

## Версии

  - ansible >= 2.8
  - python version >= 3.7
  - docker-engine >= 1.13
  - docker-compose >= 1.24

## Переменные


в defaults/main.yml
в inventory_kafka


## Запуск

`ansible-playbook -i inventory_kafka p_kafka.yml`


## Системные характеристики (ПРОД)

Рекомендуемая для прод-среды конфигурация

| Хост     | CPU, cores | RAM, GB | DISK, GB | DISK-SSD, GB |
|----------|------------|---------|----------|--------------|
| kafka01  |  16        | 64      | 1000     |              |
| kafka02  |  16        | 64      | 1000     |              |
| kafka03  |  16        | 64      | 1000     |              |
| zk01     |  4         | 4       | 500      | 512          |
| zk02     |  4         | 4       | 500      | 512          |
| zk03     |  4         | 4       | 500      | 512          |

Замечания:
  1. Данные Apache Kafka необходимо расположить на отдельном разделе/диске/томе
  2. Рекомендуемый тип рэйд-массива - 10
  3. Логи транзакций zookeeper желательно поместить на SSD
  4. Предполагается, что сервисы разнесены по разным нодам

## Системные характеристики (ТЕСТ)

Параметры тестового стенда (песочница):

| Хост     | CPU, cores | RAM, GB | DISK, GB | DISK-SSD, GB |
|----------|------------|---------|----------|--------------|
| kafka01  |  2         | 2       | 40       |              |
| kafka02  |  2         | 2       | 40       |              |
| kafka03  |  2         | 2       | 40       |              |
| zk01     |  2         | 2       | 40       | 10           |

Замечания:
  1. Предполагается, что сервисы могут находиться на одной ноде, поэтому требования по ресурсам можно не суммировать
  2. Т.к. в кластере zookeeper нет необходимости, используется один его инстанс

## Используемые образы

 * confluentinc/cp-kafka:6.2.0
 * confluentinc/cp-zookeeper:6.2.0

## Используемые переменные окружения

| Переменная                      | Тип       | Описание                                                                                              |
|---------------------------------|-----------|-------------------------------------------------------------------------------------------------------|
| KAFKA_BROKER_ID                 |основная   |уникальный id брокера kafka                                                                            |
| KAFKA_ZOOKEEPER_CONNECT         |основная   |адрес(а) zookeeper(s)                                                                                  |
| KAFKA_ADVERTISED_LISTENERS      |основная   |указывает, каким образом данный хост будет доступен для клиентских подключений (ip-адрес/hostname/fqdn)|
| KAFKA_NUM_PARTITIONS            |опционально|                                                                                                       |
| KAFKA_MIN_INSYNC_REPLICAS       |опционально|                                                                                                       |
| KAFKA_DEFAULT_REPLICATION_FACTOR|опционально|                                                                                                       |
| KAFKA_MESSAGE_MAX_BYTES         |опционально|                                                                                                       |
| KAFKA_HEAP_OPTS                 |опционально|                                                                                                       |
| ZOOKEEPER_SERVER_ID             |основная   |уникальный id ноды zookeeper                                                                           |
| ZOOKEEPER_CLIENT_PORT           |основная   |порт zookeeper для клиентских подключений                                                              |
| ZOOKEEPER_TICK_TIME             |опционально|                                                                                                       |
| ZOOKEEPER_INIT_LIMIT            |опционально|                                                                                                       |
| ZOOKEEPER_SYNC_LIMIT            |опционально|                                                                                                       |
| ZOOKEEPER_SERVERS               |основная   |список серверов кворума, с указание портов репликации                                                  |

## Проверка работоспособности

Проверить работоспособность можно, смоделировав создание топика, запись и чтение из него

1.
Создадим тестовый топик `test-topic`:

`docker run --net=host --rm confluentinc/cp-kafka:6.2.0 kafka-topics --create --topic test-topic --partitions 3 --replication-factor 3 --if-not-exists --zookeeper localhost:2181`

Результат:

`Created topic test-topic.`

2.
Проверяем, что `test-topic` создан успешно, с ReplicationFactor равным `KAFKA_DEFAULT_REPLICATION_FACTOR`:

`docker run --net=host --rm confluentinc/cp-kafka:6.2.0 kafka-topics --describe --topic test-topic --zookeeper localhost:2181`

Результат:

```
Topic: test-topic       TopicId: eJ6-99rNSMKuozqNHANQEQ PartitionCount: 3       ReplicationFactor: 3    Configs:
        Topic: test-topic       Partition: 0    Leader: 3       Replicas: 3,2,1 Isr: 3,2,1
        Topic: test-topic       Partition: 1    Leader: 1       Replicas: 1,3,2 Isr: 1,3,2
        Topic: test-topic       Partition: 2    Leader: 2       Replicas: 2,1,3 Isr: 2,1,3
```

3.
Отправим в кафку тестовые данные: например 100 чисел:

`docker run --net=host --rm confluentinc/cp-kafka:6.2.0 bash -c "seq 100 | kafka-console-producer --broker-list localhost:9092 --topic test-topic && echo 'Produced 100 messages.'"`

Результат:

`Produced 100 messages.`

4.
Теперь прочитаем их из кафки:

`docker run --net=host --rm confluentinc/cp-kafka:6.2.0 kafka-console-consumer --bootstrap-server localhost:9092 --topic test-topic --from-beginning --max-messages 100`

Результат:

```
1
...
100
Processed a total of 100 messages
```
Также создается служебный топик `__consumers_offset`

## Итого

Имеем рабочий кластер кафки на 3х брокерах